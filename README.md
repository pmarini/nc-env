### Introduction

nc-env is a tool that enables you to provision isolated Nextcloud test environments in your machine. It is built on [vagrant](https://www.vagrantup.com/) and [LXD](https://linuxcontainers.org/).

nc-env is developed and maintained by [RCA Systems](https://rcasys.com), a company dedicated to consulting and services on Nextcloud.

### Code repositories

* [RCA Systems official code repository](https://git.rcasys.com/pmarini/nc-env.git)
* [Codeberg](https://codeberg.org/pmarini/nc-env.git) (mirror)
 

### Features

You can leverage this tool to:

* Quickly setup and run an isolated Nextcloud instance in your machine.
* Try features or troubleshoot issues without any manual installation and minimal configuration.
* Learn and explore the integration of Nextcloud with common enterprise systems, such as LDAP, ElasticSearch, Single Sign-On or an Office Editing Service.

### Available Templates

nc-env is based on templates. A template is a set of instructions and artifacts sufficient to have a service up and running in a container.


| :zap:      The templates in this repository are designed and developed for testing and development environments, not for production |
|----------------------------------------------------------------------------------------------------|

The following table summarizes the list of available templates:

|Template | Description| Maintained | Vagrant Box |
| --- | --- | --- | --- |
| `template00-clean-server` | Clean server | YES | |
| `template01-nextcloud-standalone` | Nextcloud standalone server | YES | [rcasys/ubuntu2404](https://portal.cloud.hashicorp.com/vagrant/discover/rcasys/ubuntu2404) |
| `template02-collabora-online` | Collabora Online server - CODE or Enterprise | YES | [rcasys/ubuntu2404](https://portal.cloud.hashicorp.com/vagrant/discover/rcasys/ubuntu2404) |
| `template03-keycloak` | Keycloak standalone server | YES | [rcasys/ubuntu2404](https://portal.cloud.hashicorp.com/vagrant/discover/rcasys/ubuntu2404)|
| `template04-ldap` | OpenLDAP server + phpLDAPadmin | YES | [rcasys/ubuntu2404](https://portal.cloud.hashicorp.com/vagrant/discover/rcasys/ubuntu2404)|
| `template05-elasticsearch` | ElasticSearch server | YES | [rcasys/ubuntu2404](https://portal.cloud.hashicorp.com/vagrant/discover/rcasys/ubuntu2404)|
| `template06-nextcloud-db-standalone` | Database node (to be used in a cluster) | YES | |
| `template07-glusterfs-server` | GlusterFS node (to be used in a cluster) | YES | |
| `template08-haproxy-server` | HAproxy (to be used in a cluster) | YES | |
| `template09-web-server-node` | Web Server Node (to be used in a cluster) | YES | |
| `template10-redis-server` | Redis server(to be used in a cluster) | YES | |
| `template11-minio-storage-server` | MinIO Storage Server | YES | |
| `template12-lookup-server` | Nextcloud lookup server | YES | |
| `template13-talk-hpb` | Talk High Performance Backend | YES | |
| `template14-self-hosted-appstore` | Nextcloud Self-Hosted Appstore | NO | |
| `template15-notify-push-server` | Nextcloud Notify Push Server (a.k.a. Files HPB) | YES | |
| `template16-zabbix-server` | Zabbix Monitoring Server | YES | |
| `template17-mail-server` | Mox Mail Server | YES | |



### Setup

This section aims at giving an high-level procedure to setup your environment.

Please head to the `how-to` folder to find step-by-step guides for the most common Linux Desktop distributions ([Fedora](./how-to/How-To-Setup-Nc-Env-In-Fedora-Workstation.md), [Ubuntu](./how-to/How-To-Setup-Nc-Env-In-Ubuntu-Desktop.md), [Debian](./how-to/How-To-Setup-Nc-Env-in-Debian-Bullseye.md)).

1. Install and configure LXD
2. Configure the host to resolve container hostnames
3. Install and configure vagrant
4. Install LXD plugin vagrant-lxd
5. Install mkcert
6. Get the latest release of nc-env
7. Start provisioning

A Virtual Machine is also available for those that want to have a fully isolated environment, instructions are available [here](./how-to/How-To-Setup-the-nc-env-Virtual-Machine.md).

### Resources

* Article: [nc-env: A tool to deploy Nextcloud Environments in your laptop](https://propuestas.eslib.re/2022/miscelanea/nc-env-creacion-entornos-nextcloud-local).
* Video: [This workshop](https://commons.wikimedia.org/wiki/File:EsLibre_2022_P24_-_Pietro_Marini_-_C%C3%B3mo_montar_un_cl%C3%BAster_de_Nextcloud.webm), recorded at [esLibre 2022](https://eslib.re/2022/), is focused on how to build a Nextcloud cluster using nc-env. In Spanish.
* Video: [This workshop](https://commons.wikimedia.org/wiki/File:EsLibre_2023_P58_-_Pietro_Marini_-_nc-env_%E2%80%93_una_herramienta_para_montar_entornos_Nextcloud_en_tu_ordenador.webm), recorded at [esLibre 2023](https://eslib.re/2023/), is a walkthrough on how to setup nc-env in your laptop. In Spanish.

