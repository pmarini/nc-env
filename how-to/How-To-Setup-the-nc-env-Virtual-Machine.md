# How To Setup the nc-env Virtual Machine

## Introduction

While it is advised to run nc-env directly in your machine, we also provide a virtual machine image
with a pre-configured nc-env environment. 

It is a Ubuntu Desktop system, that means that you have a graphical interface in it.

## Pre-requisites

A machine where you can run LXD daemon (Linux or macOS, as Windows only supports the LXD client), with at least 30 GB of free space.

## Downloads 

Download the virtual machine and the storage volume from:

https://chat.x51-hserver.com/s/aajB2E9PeKSAXqR


## Configure your host

1. Install lxd. See the official [documentation](https://linuxcontainers.org/lxd/getting-started-cli/#installing-a-package).
2. Initialize lxd. See the official [documentation](https://linuxcontainers.org/lxd/docs/latest/howto/initialize/).
3. Create a storage pool called `nc-env-vm-pool`.

	If you have a free device or partition:

	```
	$ lxc storage create nc-env-vm-pool zfs source=/dev/<name of device> zfs.pool_name=nc-env-vm-pool
	```

	Otherwise:

	```
	$ fallocate -l 30GB nc-env-vm-pool.disk
	## Find the first unused device
	$ sudo losetup -f
	/dev/loop19
	$ sudo losetup /dev/loop19 nc-env-vm-pool.disk
	$ lxc storage create nc-env-vm-pool zfs source=/dev/loop19 zfs.pool_name=nc-env-vm-pool
	```

4. Create the profile `prf-nc-env-vm`:

	```
	$ lxc profile create nc-env-vm-prf
	$ lxc profile set nc-env-vm-prf boot.autostart false
	$ lxc profile set nc-env-vm-prf limits.memory 6GiB
	$ lxc profile set nc-env-vm-prf limits.cpu 4
	$ lxc profile device add nc-env-vm-prf root disk path=/ size=20GB pool=nc-env-vm-pool
	$ lxc profile device add nc-env-vm-prf eth0 nic nictype=bridged parent=lxdbr0 name=eth0
	```

5. Import the virtual machine:

	```
	$ lxc import nc-env-vm.tar.bz2
	```
	
6. Import the volume called `nc-env-vm-storage` and attach it to the virtual machine:

	```
	$ lxc storage volume import nc-env-vm-pool nc-env-vm-storage.tar.bz2
	$ lxc storage volume attach nc-env-vm-pool nc-env-vm-storage nc-env-vm
	```

7. Start the virtual machine and open a graphical console into it:

	```
	$ lxc start nc-env-vm
	$ lxc console --type vga nc-env-vm
	```

	
## Important Virtual Machine Information

* When connecting with the graphical console you will be using user `ubuntu`, with password `ubuntu`
* An alternative way to connect to the virtual machine is via `lxc exec nc-env-vm bash`, in this case you
  will be connected with `root`
* The VM comes with the latest nc-env stable version, available in the user home folder
* When using a loop device for the storage pool you need to launch the following commands at each host machine
  reboot

```
$ sudo losetup /dev/loop19 nc-env-vm-pool.disk
$ sudo zpool import nc-env-vm-pool
```

	
You are now ready to create your first container-based Nextcloud environment!
