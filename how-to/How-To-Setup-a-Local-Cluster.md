## How to Create the Cluster

Create a project in LXD, for example `nc-cluster`

```
lxc project create nc-cluster -c features.images=false -c features.profiles=false
```

### Database (MariaDB)

1. Use template `template06-nextcloud-db-standalone` (follow instructions in Readme.md).
2. Move the container to the project `nc-cluster`

	```
	lxc move db-server --target-project nc-cluster

	```

  Moving the container will shut it down. Restart it.
3. Add the DNS entry in the /etc/hosts file. The IP address is reported in the tail of the provision log.
4. Test: to test the database is setup and running you can

	* open an ssh session into the container and run: systemctl status mysql 

	* connect to the database using the credentials specified in the tail of the log

### Cache (Redis)

1. Use template `template10-redis-server` (follow instructions in Readme.md)
2. Move the container to the project `nc-cluster`
	
	```
	lxc move redis-server --project default  --target-project nc-cluster
	```
	
  Moving the container will shut it down. Restart it.
3. Add the DNS entry in the `/etc/hosts` file. The IP address is reported in the tail of the provision log.
4. Test: to test the Redis is setup and running you can
	
	* open an ssh session and launch the following commands	
	```
	usrv@redis-server:~$ redis-cli
	127.0.0.1:6379> ping "Test"
	"Test"
	127.0.0.1:6379> 
	usrv@redis-server:~$ systemctl status redis
	```


### Storage (GlusterFS)

1. Use template `template07-glusterfs-server`.
2. Move the container to the project `nc-cluster`
	
	```
	lxc move glusterfs-server --target-project nc-cluster
	```
   
   Moving the container will shut it down. Restart it.
3. Add the DNS entry in the `/etc/hosts` file. The IP address is reported in the tail of the provision log.  
4. Test: to test that GlusterFS is setup and running you can install the glusterfs-client in your host machine and try to mount
   the volume `vol01`:
   
    ```
    $ ls
    gluster-test
    $ sudo mount -t glusterfs glusterfs-server.localenv.com:/vol01 gluster-test
    $ df -hT gluster-test/
    Filesystem                           Type            Size  Used Avail Use% Mounted on
    glusterfs-server.localenv.com:/vol01 fuse.glusterfs  245G  8,4G  236G   4% /gluster-test
    ``` 


### Web Server Nodes (Apache)

For each node:

1. Use template `template09-web-server-node` (follow instructions in Readme.md).
2. Move the container to the project `nc-cluster`.
	
	```
	lxc move web-server-0n --target-project nc-cluster
	``` 
   Moving the container will shut it down. Restart it.
3. Add the DNS entry in the `/etc/hosts` file. The IP address is reported in the tail of the provision log.
4. The mount operation is managed via the `data-nextcloud.mount` systemd service, enabled at boot. The mount point is at `/data/nextcloud`
5. Test: to test, connect to each node in the browser and login with admin/admin.


### Haproxy Server

1. Use `template08-haproxy-server` (follow instructions in Readme.md).
2. Move the container to the project `nc-cluster`.

	```
	lxc move haproxy-server --target-project nc-cluster
	``` 
	
   Moving the container will shut it down. Restart it.
3. Add the DNS entry in the `/etc/hosts` file. The IP address is reported in the tail of the provision log.
4. Test: to test the HAProxy setup you can
   * open an ssh session and launch the following commands	

## How to Start the Cluster

```
lxc switch project nc-cluster

lxc start database-server

lxc start glusterfs-server

lxc start redis-server

lxc start web-server-01

lxc start web-server-02

lxc ls
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
|       NAME       |  STATE  |        IPV4        |                    IPV6                     |   TYPE    | SNAPSHOTS |
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
| db-server        | RUNNING | 10.23.46.79 (eth0) | fd42:fa6c:5eee:12:216:3eff:fe69:d1ea (eth0) | CONTAINER | 0         |
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
| glusterfs-server | RUNNING | 10.23.46.48 (eth0) | fd42:fa6c:5eee:12:216:3eff:fed0:a5cf (eth0) | CONTAINER | 0         |
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
| haproxy-server   | RUNNING | 10.23.46.97 (eth0) | fd42:fa6c:5eee:12:216:3eff:fe5b:6841 (eth0) | CONTAINER | 0         |
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
| redis-server     | RUNNING | 10.23.46.13 (eth0) | fd42:fa6c:5eee:12:216:3eff:fefc:618c (eth0) | CONTAINER | 0         |
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
| web-server-01    | RUNNING | 10.23.46.12 (eth0) | fd42:fa6c:5eee:12:216:3eff:fe3b:a06b (eth0) | CONTAINER | 0         |
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
| web-server-02    | RUNNING | 10.23.46.21 (eth0) | fd42:fa6c:5eee:12:216:3eff:fe45:5f41 (eth0) | CONTAINER | 0         |
+------------------+---------+--------------------+---------------------------------------------+-----------+-----------+
```
