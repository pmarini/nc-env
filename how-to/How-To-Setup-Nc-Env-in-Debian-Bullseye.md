# Initial Setup of nc-env in Debian 11

## Introduction

This document describes the step-by-step procedure for the initial setup of nc-env in a Debian 11 system.

Check the [project homepage](https://codeberg.org/pmarini/nc-env) to know more about nc-env.

## Software Environment



| Component      |Version                         |
|----------------|--------------------------------|
|Operating System|Debian GNU/Linux 11 (bullseye)|
|LXD             |5.0.0                           |
|vagrant         |2.2.19                          |
|vagrant-lxd     |0.5.6                           |



## Procedure

Install all the software needed for the project, starting by updating the package repositories:

```
$ sudo apt update
```

The following packages are not strictly needed, but may make the following operations more agile, depending on the user tooling preferences.

```
$ sudo apt install vim tree
```

### LXD Installation

LXD will be installed as a snap, and snap is not pre-installed in the system. So let's install snap first, then install the required snap `core`.


```
$ sudo apt install snapd
```


```
$ sudo snap install core

```


Now you can install LXD using snap:

```
$ sudo snap install lxd --channel=5.0/stable
lxd 5.0.0-b0287c1 from Canonical✓ installed
```

Verify that LXD has been correctly installed:

```
$ snap list lxd
Name  Version        Rev    Tracking       Publisher   Notes
lxd   5.0.0-b0287c1  22923  latest/stable  canonical✓  -
```

### ZFS Utilities Installation

---

:memo: You can find more extensive ZFS setup instructions [here](https://linuxhint.com/install-zfs-debian/).

---

---

:memo: While it is recommeded to use ZFS as backend for the LXD storage pools for this project, you can skip this section if you plan to use directory-based LXD storage pools.

---

If you plan to use ZFS as  backend for the LXD storage pools, you should have a disk or unformatted partition, of 20GB or more, for that. 

Add the repository where the ZFS packages live:

```
$ sudo apt-add-repository contrib
```

Then install the package `zfsutils-linux`:

```
$ sudo apt update

$ sudo apt install zfsutils-linux
```

You can check the the package has been successfully installed by running the following command:

```
$ sudo zpool --version
zfs-2.0.3-9
zfs-kmod-2.0.3-9
```

### Vagrant Installation

Install `curl`:

```
$ sudo apt install curl
```

Get the Hashicorp signing key into the system (instructions taken from the official [Hashicorp Vagrant download page](https://www.vagrantup.com/downloads)):

```
$ sudo curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
```

Add the official repository for `vagrant`:

```
$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
$ sudo apt update
```


Install the vagrant package:

```
$ sudo apt install vagrant

```

Verify that it has been correctly installed:

```
$ vagrant --version
Vagrant 2.2.19
```

### Vagrant-lxd plugin Installation

Vagrant plugins are installed by using the `plugin` switch, so let's install the `vagrant-lxd` plugin by issuing the following command:

```
$ vagrant plugin install vagrant-lxd
Installing the 'vagrant-lxd' plugin. This can take a few minutes...
Fetching thread_safe-0.3.6.gem
Fetching tzinfo-1.2.9.gem
Fetching minitest-5.15.0.gem
Fetching activesupport-5.2.7.gem
Fetching multipart-post-2.1.1.gem
Fetching faraday-0.17.5.gem
Fetching public_suffix-4.0.7.gem
Fetching addressable-2.8.0.gem
Fetching sawyer-0.9.1.gem
Fetching hyperkit-1.3.0.gem
Fetching vagrant-lxd-0.5.6.gem
Installed the plugin 'vagrant-lxd (0.5.6)'!
```

Verify that it has been correctly installed:

```
$ vagrant plugin list
vagrant-lxd (0.5.6, global)
```

As recommended in the vagrant-lxd documentation ([here](https://gitlab.com/catalyst-it/devtools/vagrant-lxd#synced-folders)), to ensure synced folders work as expected, run the following commands:

```
$ echo root:$(id -u):1 | sudo tee -a /etc/subuid
$ echo root:$(id -g):1 | sudo tee -a /etc/subgid
```

### Mkcert Installation

Download from [the official repository](https://github.com/FiloSottile/mkcert/releases) the latest released binary of mkcert, an utility that makes it easy to create locally trusted TLS certificates:

```
$ pwd
/home/fedora_usr/Documents/nc-env/
$ mkdir my-local-env
$ cd my-local-env
$ wget https://github.com/FiloSottile/mkcert/releases/download/v1.4.4/mkcert-v1.4.4-linux-amd64
```

Install a needed dependency for mkcert:

```
$ sudo dnf install nss-tools
```

Rename the mkcert binary by stripping the version:

```
$ mv mkcert-v1.4.4-linux-amd64 mkcert
```


Make it executable:

```
$ chmod +x mkcert
```

Run the install command

```
$ ./mkcert -install
The local CA is already installed in the system trust store! 👍
The local CA is now installed in the Firefox and/or Chrome/Chromium trust store (requires browser restart)! 🦊
```

Check the local CAROOT folder by issuing the following command. We will need to make the files in that folder available to the containers as well:

```
$ ./mkcert -CAROOT
```

### Nc-env Installation

Pick the latest release of the project from [here](https://codeberg.org/pmarini/nc-env/releases), in this example `v20220421`:

```
$ wget https://codeberg.org/pmarini/nc-env/archive/v20220421.tar.gz
$ tar -xzf v20220421.tar
$ cd nc-env/
```

### Next steps

Please refer to section "LXD Configuration" in guide How-To-Setup-Nc-Env-In-Ubuntu-Desktop.md for the following steps.
