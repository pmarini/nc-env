## How to Setup Global Scale


### Introduction

This step-by-step tutorial gives you the procedure to setup a basic Global Scale setup in your machine, with a Lookup Server, a Global Site Selector Node and 2 Global Scale Nodes.

To easily follow this document, please find here the container names and their fully qualified domain names. These are just sample names, and you can change them provided you change them in all the files where they need to be changed.

| Component | Container Name| Fully Qualified Domain Name|
| --- | --- | --- |
| Lookup Server | `gs-lookup` | `gs-lookup.localenv.com` |
| Global Site Selector Node |`gs-selector`| `gs-selector.localenv.com` |
| Global Scale Node 1 | `gs-node01` | `gs-node01.localenv.com` |
| Global Scale Node 2 | `gs-node02` | `gs-node02.localenv.com` |

### Create a LXD project

Create a folder to use for the Global Scale setup and navigate into it:

```
mkdir global-scale
cd global-scale
```

Create a project in LXD called `global-scale`:
	
```
lxc project create global-scale -c features.images=false -c features.profiles=false
```

### Install the Lookup Server

1. Use template `template12-lookup-server` (follow instructions in `Readme.md`).

2. Move the container to the project `global-scale`:

	```
	lxc move gs-lookup --target-project global-scale
	```

  Moving the container will shut it down. Restart it.

3. Add the DNS entry in the `/etc/hosts` file. The IP address is reported in the tail of the provision log.

4. You can do a simple test to check whether the Lookup Server is up and running, that is issueing the following wget request from your host machine:
	```
	wget -O /dev/null https://gs-lookup.localenv.com/index.php/status
	```
   You must get a 200 return code. This test can be repeated from the other components (Global Site Selector Node, Global Scale Nodes) once setup, as they require connectivity to the Lookup Server.

### Install the Global Site Selector Node

1. Use template `template01-nextcloud-standalone` (follow instructions in `Readme.md`).

2. Move the container to the project `global-scale`:
	
	```
	lxc move gs-selector --project default  --target-project global-scale
	```
	
  Moving the container will shut it down. Restart it.

3. Add the DNS entry in the `/etc/hosts` file. The IP address is reported in the tail of the provision log.

4. Connect via ssh to the container and issue the following commands:
	
	```
	sudo -u www-data php /var/www/nextcloud/occ app:enable globalsiteselector
	sudo -u www-data php /var/www/nextcloud/occ config:system:set lookup_server --value 'https://gs-lookup.localenv.com/index.php' 
	sudo -u www-data php /var/www/nextcloud/occ config:system:set gs.enabled --type boolean --value true
	sudo -u www-data php /var/www/nextcloud/occ config:system:set gss.jwt.key --value 'random-key'
	sudo -u www-data php /var/www/nextcloud/occ config:system:set gss.mode --value 'master'
	sudo -u www-data php /var/www/nextcloud/occ config:system:set gss.master.admin 0 --value 'admin'
	```

5. At this point you can just test that you can login with `admin` to the Nextcloud Web UI using the following URL: `https://gs-selector.localenv.com/index.php`.


### Install 2 Global Scale Nodes

1. Use template `template01-nextcloud-standalone` (follow instructions in `Readme.md`).

2. Move the container to the project `global-scale`:
	
	```
	lxc move gs-node --target-project global-scale
	```
   
   Moving the container will shut it down. Restart it.

3. Add the DNS entry in the `/etc/hosts` file. The IP address is reported in the tail of the provision log.

4. Connect via ssh to the container and issue the following commands:

	```
	sudo -u www-data php /var/www/nextcloud/occ app:enable globalsiteselector
	sudo -u www-data php /var/www/nextcloud/occ config:system:set  lookup_server --value  'https://gs-lookup.localenv.com/index.php'
	sudo -u www-data php /var/www/nextcloud/occ config:system:set  gs.enabled --type boolean --value true
	sudo -u www-data php /var/www/nextcloud/occ config:system:set  gss.jwt.key --value 'random-key'
	sudo -u www-data php /var/www/nextcloud/occ config:system:set  gss.mode --value 'slave'
	sudo -u www-data php /var/www/nextcloud/occ config:system:set  gss.master.url --value  'https://gs-selector.localenv.com'
	```

5. To test that everything is working you can:
	* create a user in the node by connecting directly to the node with `admin`. To bypass the Global Site Selector redirection use the `direct=1` parameter: `https://gs-node01.localenv.com/index.php/login?direct=1`
	* logout from the node. The browser should be redirected to `gs-selector.localenv.com`
	* login with the user just created. The user should be allowed in and redirected to node `gs-node01`.

6. Repeat these steps for `gs-node02`

## How to Start the Global Scale installation

You can start the Global Scale with the following commands:


```
lxc switch project global-scale

lxc start gs-lookup

lxc start gs-selector

lxc start gs-node01

lxc start gs-node02

lxc ls
+-------------+---------+---------------------+---------------------------------------------+-----------+-----------+
|    NAME     |  STATE  |        IPV4         |                    IPV6                     |   TYPE    | SNAPSHOTS |
+-------------+---------+---------------------+---------------------------------------------+-----------+-----------+
| gs-lookup   | RUNNING | 10.23.46.172 (eth0) | fd42:fa6c:5eee:12:216:3eff:fe18:bf2e (eth0) | CONTAINER | 0         |
+-------------+---------+---------------------+---------------------------------------------+-----------+-----------+
| gs-node01   | RUNNING | 10.23.46.32 (eth0)  | fd42:fa6c:5eee:12:216:3eff:feb4:d046 (eth0) | CONTAINER | 0         |
+-------------+---------+---------------------+---------------------------------------------+-----------+-----------+
| gs-node02   | RUNNING | 10.23.46.159 (eth0) | fd42:fa6c:5eee:12:216:3eff:fe27:6f58 (eth0) | CONTAINER | 0         |
+-------------+---------+---------------------+---------------------------------------------+-----------+-----------+
| gs-selector | RUNNING | 10.23.46.188 (eth0) | fd42:fa6c:5eee:12:216:3eff:fec2:f2d4 (eth0) | CONTAINER | 0         |
+-------------+---------+---------------------+---------------------------------------------+-----------+-----------+
```
