### Stand-alone Nextcloud server

#### Setup

* Assuming that the copy of the template is called `mail-server`, move to folder `mail-server`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `rootCA.pem` | The rootCA previously created in your host machine |                                                                                                                                                                                    
| `rootCA-key.pem` | The rootCA key previously created in your host machine |
| mox | The program binary [here](https://beta.gobuilds.org/github.com/mjl-/mox) |
| domains.conf | Domain configuration file for Mox |
| mox.conf | Main configuration file for Mox |
| mox.service | Systemd service unit file for Mox |


* Create folder `log`
* Open `Vagrantfile` and change the value of variable `lxd.name`. It makes sense to give the same name as the folder, in this example `mail-server`.
* Open `provision.sh`
   * change the value of variable `MACHINE_HOSTNAME`. It makes sense to give the same name as the folder, plus the domain, in this example `mail-server.localenv.com`.
   * change the value of variable `MAIL_DOMAIN`.
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
