
### Collabora Online server (CODE or Enterprise Edition)

#### Setup

* Assuming that the copy of the template is called `collabora-instance`, move to folder `collabora-instance`.
* Check the content of folder `artifacts`


|File name | Description|
| --- | --- |
| `collaboraonline.sources` | Apt sources file for Collabora packages |
| `coolwsd.xml` | Configuration file for the Collabora server |
| `coolwsd.service` | Systemd service file for Collabora server |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |


* Create folder `log`
* Open `Vagrantfile` and assign values to the following parameters or variables:
    * `lxd.name` (it is recommended to give the same name as the folder, in this example `collabora-instance`)
    * `MACHINE_HOSTNAME`
    * `CO_VERSION` 
    * `CUSTOMER_HASH` (only for Enterprise Edition)
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or configure a nameserver in LXD
* Start using your environment
