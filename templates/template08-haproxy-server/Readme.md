### Stand-alone HAProxy Server

#### Setup

* Assuming that the copy of the template is called `haproxy-server`, move to folder `haproxy-server`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| haproxy.cfg | haproxy configuration file |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |



* Create folder `log`
* Open `Vagrantfile` and assign a value the following variables and parameters 
   * `lxd.name` (it is recommended to give the same name as the folder, in this example `haproxy-server`)
   * `MACHINE_HOSTNAME`
   * `WEBSERVER_NODE01_HOSTNAME`
   * `WEBSERVER_NODE02_HOSTNAME`
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
