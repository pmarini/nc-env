### Stand-alone Nextcloud server

#### Setup

* Assuming that the copy of the template is called `minio-storage-server`, move to folder `minio-storage-server`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| mc | Minio Client's binary file - Downloadable from https://dl.min.io/client/mc/release/linux-amd64/ |
| minio | Minio Server's binary file - Downloadable from https://dl.min.io/server/minio/release/linux-amd64/minio |
| minio.service | Minio Server unit file - Base file is https://raw.githubusercontent.com/minio/minio-service/master/linux-systemd/minio.service |
| minio.conf |Minio Server configuration file |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |

* Create folder `log`
* Open `Vagrantfile` and assign a value to the following parameters and variables:
   * `lxd.name` (it is recommended to give the container the same name as the folder, in this example `minio-storage-server`)
   * `MACHINE_HOSTNAME`

* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or by setting up the LXD nameserver
* Start using your environment

