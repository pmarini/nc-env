### ElasticSearch server

| :zap:      As of August 2023, Full Text Search on Nextcloud 26+ is compatible with ElasticSearch 8. Previous versions are only compatible with ElasticSearch 7|
|----------------------------------------------------------------------------------------------------|


| :zap: Elastic Search is quite resource-intensive so that you may want to create a dedicated LXD profile and assign the container to that profile (property `lxd.profiles`) |
|----------------------------------------------------------------------------------------------------|


#### Setup

* Assuming that the copy of the template is called `elasticsearch-instance`, move to folder `elasticsearch-instance`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `elasticsearch-${ELASTICSEARCH_VERSION}-amd64.deb` | Debian archive for version 8.X.Y to be downloaded from [here](https://www.elastic.co/downloads/elasticsearch) |
| `memory.options` | Limit the total memory assigned to the JVM |
| `elasticsearch.yml` | ElasticSearch core configuration file  |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |



* Open `Vagrantfile` and assign a value to the following variables and parameters:
   * `lxd.name` (it is recommended to give the same name as the folder, in this example `elasticsearch-instance`)
   * `MACHINE_HOSTNAME`
   * `ELASTICSEARCH_VERSION`
* Create folder `log`
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or by setting up the LXD nameserver
* Start using your environment
