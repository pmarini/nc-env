### Stand-alone Nextcloud server

#### Setup

* Assuming that the copy of the template is called `clean-server`, move to folder `clean-server`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |


* Create folder `log`
* Open `Vagrantfile` and change the value of variable `lxd.name`. It makes sense to give the same name as the folder, in this example `clean-server`.
* Open `provision.sh`
   * change the value of variable `MACHINE_HOSTNAME`. It makes sense to give the same name as the folder, plus the domain, in this example `clean-server.localenv.com`.
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
