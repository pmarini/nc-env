### Stand-alone MariaDB Database for a Nextcloud cluster

#### Setup

* Assuming that the copy of the template is called `nc-db-instance`, move to folder `nc-db-instance`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `50-server.cnf` | The MariaDB server configuration file |


* Create folder `log`
* Open `Vagrantfile` and assign a value to the following parameters and variables:
   * `lxd.name` (it is recommended to give the container the same name as the folder, in this example `nc-db-instance`)
   * `MACHINE_HOSTNAME`
   * `NEXTCLOUD_DB_NAME`
   * `NEXTCLOUD_DB_USER`
   * `NEXTCLOUD_DB_PASSWORD`

* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
