### Zabbix Server for monitoring

#### Setup

* Assuming that the copy of the template is called `zabbix-server`, move to folder `zabbix-server`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `pg_hba.conf` | Postgres db configuration file |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |
| `zabbix.conf.php` | Zabbix PHP configuration file |
| `zabbix-release_X.Y-Z+ubuntu22.04_all.deb` | Apt repository installation package. Package picker available [here](https://www.zabbix.com/download?zabbix=6.4&os_distribution=ubuntu&os_version=22.04&components=server_frontend_agent&db=pgsql&ws=apache). Pick the Apache2/Postgres combination |
| `zabbix_server.conf` | Main Zabbix configuration file |
| `zabbix_virtualhost.conf` | Apache Virtualhost file for Zabbix Web Interface |


* Create folder `log`
* Open `Vagrantfile` and change the value of variable and parameters 
   * `lxd.name` (it is recommended to give the same name as the folder, in this example `zabbix-server`)
   * `MACHINE_HOSTNAME` (it is recommended to give the same name as the folder, plus the domain, in this example `zabbix-server.localenv.com`)
   * `ZABBIX_DEB_PACKAGE`
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
