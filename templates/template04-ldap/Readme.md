### OpenLDAP server

#### Setup

* Assuming that the copy of the template is called `ldap-instance`, move to folder `ldap-instance`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `debconf-slapd.conf | Pre-filled answers to questions for silent installation of slapd |
| `lam.conf| Main configuration file for [LDAP Account Manager](https://www.ldap-account-manager.org/lamcms) |
| `ldap-account-manager.conf` | Apache virtual host file |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |


* Open `Vagrantfile` and fill in the values of the following variables parameters:
   * `lxd.name`: The name of the container (it is recommended to give the same name as the folder, in this example `nc-instance`
   * `MACHINE_HOSTNAME`: The hostname of the container (it is recommended to give the same name as the folder, plus the domain, in this example `nc-instance.localenv.com`
* Create folder `log`
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
