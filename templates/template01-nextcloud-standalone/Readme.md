### Stand-alone Nextcloud server

#### Setup

* Assuming that the copy of the template is called `nc-instance`, move to folder `nc-instance`.
* Check the content of folder `artifacts`:

|File name | Description|
| --- | --- |
| `nc-fulltext-live-indexer.service` | OPTIONAL - systemd unit file for the Fulltext live indexer |
| `nc-redirect.conf` | Apache Web Server http-to-https redirect configuration file |
| `nextcloud.conf` | Apache Web Server main configuration file |
| `nextcloud-X.Y.Z.tar.bz2` | The installer archive to be downloaded from [here](https://download.nextcloud.com/server/releases/) |
| `nextcloud-X.Y.Z.tar.bz2.md5` | md5 signature of the installer archive to be downloaded from [here](https://download.nextcloud.com/server/releases/) |
| `occ` | Alias for the occ command |
| `redis.conf` | Redis server configuration file |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |

* Open `Vagrantfile` and fill in the values of the following parameters:
   * `lxd.name`: The name of the container (it is recommended to give the same name as the folder, in this example `nc-instance`.
   * `MACHINE_HOSTNAME`: The hostname of the container (it is recommended to give the same name as the folder, plus the domain, in this example `nc-instance.localenv.com`.
   * `NEXTCLOUD_INSTALLER_ARCHIVE`: The name of the installer archive you are using.
* Create folder `log`
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or configure LXD nameserver
* Start using your environment
