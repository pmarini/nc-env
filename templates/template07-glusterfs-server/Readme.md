### Stand-alone GlusterFs Node to be used as storage for a Nextcloud cluster

#### Setup

| :zap:     The container is required to be privileged. The configuration switch is already enabled in Vagrantfile.|
|----------------------------------------------------------------------------------------------------|

* Assuming that the copy of the template is called `glusterfs-instance`, move to folder `glusterfs-instance`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |

* Create folder `log`
* Open `Vagrantfile` and assign a value to the following parameters and variables:
   * `lxd.name` (it is recommended to give the container the same name as the folder, in this example `nc-db-instance`)
   * `MACHINE_HOSTNAME`

* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
