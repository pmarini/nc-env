#!/bin/bash

timedatectl set-timezone Europe/Madrid

start_time=`date`
  
echo "provisioning started: ${start_time}" 

NETWORK_INTERFACE=eth0

hostnamectl set-hostname ${MACHINE_HOSTNAME}

# Print some information about the container OS
hostnamectl

# Print some information about the container timezone
timedatectl

#####################################################################
## Get the IP address into an environment variable. This command outputs 
## an empty variable if the network interface name is not ${NETWORK_INTERFACE}
#####################################################################
ip_address=`ip -4 addr show ${NETWORK_INTERFACE} | grep -oP '(?<=inet\s)\d+(\.\d+){3}'`

apt update

apt install -y glusterfs-server

systemctl start glusterd

mkdir /srv/glusterfs-volume

gluster volume create vol01 ${MACHINE_HOSTNAME}:/srv/glusterfs-volume force

gluster volume start vol01

systemctl enable glusterd

systemctl status glusterd

gluster volume info vol01

echo "START - Cleanup"

rm -rf /vagrant/artifacts/*

echo "END - Cleanup"

end_time=`date`

echo "This container has IP (interface: ${NETWORK_INTERFACE}): ${ip_address}"

echo "provisioning started: ${start_time}" 
 
echo "provisioning ended: ${end_time}"
