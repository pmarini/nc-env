### Stand-alone Redis server

#### Setup

* Assuming that the copy of the template is called `redis-server`, move to folder `redis-server`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| redis.conf | redis configuration file


* Create folder `log`
* Open `Vagrantfile` and assign a value to the following parameters and variables:
   * `lxd.name` (it is recommended to give the container the same name as the folder, in this example `redis-server`)
   * `MACHINE_HOSTNAME`

* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or by setting up the LXD nameserver
* Start using your environment
