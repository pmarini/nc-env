#!/bin/bash

timedatectl set-timezone Europe/Madrid

start_time=`date`
  
echo "provisioning started: ${start_time}" 

NETWORK_INTERFACE=eth0

hostnamectl set-hostname ${MACHINE_HOSTNAME}

# Print some information about the container OS
hostnamectl

# Print some information about the container timezone
timedatectl

#####################################################################
## Get the IP address into an environment variable. This command outputs 
## an empty variable if the network interface name is not ${NETWORK_INTERFACE}
#####################################################################
ip_address=`ip -4 addr show ${NETWORK_INTERFACE} | grep -oP '(?<=inet\s)\d+(\.\d+){3}'`

adduser --disabled-password --gecos GECOS ${SYSTEM_USER}
 
usermod -p "`openssl passwd -1  -salt 5RPVAd ${SYSTEM_USER}`" ${SYSTEM_USER}
 
adduser ${SYSTEM_USER} sudo

# https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04
apt update

apt install -y redis-server net-tools

cp /vagrant/artifacts/redis.conf /etc/redis/redis.conf

systemctl restart redis-server

systemctl status redis-server

netstat -lnp | grep redis

adduser usrv redis

end_time=`date`

echo "This container has IP (interface: ${NETWORK_INTERFACE}): ${ip_address}"

echo "provisioning started: ${start_time}" 
 
echo "provisioning ended: ${end_time}"
