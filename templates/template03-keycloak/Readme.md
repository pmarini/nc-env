### Keycloak server

#### Setup

* Assuming that the copy of the template is called `keycloak-instance`, move to folder `keycloak-instance`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `keycloak-X.Y.Z.tar.gz` | The installer archive to be downloaded from [here](https://www.keycloak.org/downloads.html) - Distribution powered by Quarkus |
| `keycloak.service` | The systemd unit file for Keycloak service |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |
| `keycloak-env.conf` | The environment file for the systemd service |
| `keycloak.conf` | The Keycloak configuration file ||


* Open `Vagrantfile` and a value to the following variables and parameters: 
	* `lxd.name` (it is recommended to give the same name as the folder, in this example `keycloak-instance`)
	* `MACHINE_HOSTNAME`
	* `KEYCLOAK_VERSION`
* Create folder `log`
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or setting up the LXD nameserver
* Start using your environment
