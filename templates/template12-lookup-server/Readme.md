### Lookup server - meant to be deployed in a Global Scale

#### Setup

* Assuming that the copy of the template is called `gs-lookup-server`, move to folder `gs-lookup-server`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| config.php | Main configuration file for the Lookup Server |
| lookup-server.conf | Apache Virtual Host configuration file |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |
| `lookup-server-1.1.0.tar.gz` | Release archive of the Lookup Server, downloadable from [here](https://github.com/nextcloud/lookup-server/tags) | 


* Create folder `log`
* Open `Vagrantfile` and change the value of the following variables and parameters:
 
   * `lxd.name` (it is recommended to give the same name as the folder, in this example `gs-lookup-server`)
   * `MACHINE_HOSTNAME`
   * `LOOKUP_SERVER_INSTALLER`
   * `LOOKUP_SERVER_VERSION`

* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
