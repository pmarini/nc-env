#!/bin/bash

timedatectl set-timezone Europe/Madrid

start_time=`date`
  
echo "provisioning started: ${start_time}" 

PHP_VERSION=8.1

NETWORK_INTERFACE=eth0

LOOKUP_DATABASE_NAME=lookup_server_db

LOOKUP_DATABASE_USER=lookup_server_usr

hostnamectl set-hostname ${MACHINE_HOSTNAME}

# Print some information about the container OS
hostnamectl

# Print some information about the container timezone
timedatectl

#####################################################################
## Get the IP address into an environment variable. This command outputs 
## an empty variable if the network interface name is not ${NETWORK_INTERFACE}
#####################################################################
ip_address=`ip -4 addr show ${NETWORK_INTERFACE} | grep -oP '(?<=inet\s)\d+(\.\d+){3}'`

if [ "${PHP_VERSION}" == "7.4" ]; then

        apt update

        apt install -y software-properties-common apt-transport-https

        add-apt-repository ppa:ondrej/php -y

fi

## Install the needed packages from apt repositories
apt update

apt install -y apache2 mariadb-server libapache2-mod-php${PHP_VERSION} php${PHP_VERSION}-mysql unzip wget mkcert make composer php-curl

export CAROOT=/vagrant/artifacts/

mkcert -install

mkcert  --cert-file /etc/ssl/certs/${MACHINE_HOSTNAME}.pem --key-file /etc/ssl/private/${MACHINE_HOSTNAME}-key.pem "${MACHINE_HOSTNAME}"

#################
## Expand the installer archive
#################
tar -xf /vagrant/artifacts/${LOOKUP_SERVER_INSTALLER} -C /vagrant/artifacts/

#################
## Database
#################

echo "START - Create the database"

mysql -u root -e "create database ${LOOKUP_DATABASE_NAME};"

echo "Create the database -> Create the user"
mysql -u root -e "create user '${LOOKUP_DATABASE_USER}'@'localhost' identified by '${LOOKUP_DATABASE_USER}'"

echo "Create the database -> Grant to the user all the privileges on the database"
mysql -u root -e "grant all privileges on ${LOOKUP_DATABASE_NAME}.* to '${LOOKUP_DATABASE_USER}'@'localhost'"

## Putting the name of the database inside the tables creation script
sed -i 's|`lookup`|`'"${LOOKUP_DATABASE_NAME}"'`|g' /vagrant/artifacts/lookup-server-${LOOKUP_SERVER_VERSION}/mysql.dmp

echo "Create the database -> Execute the DDL script"
mysql -u${LOOKUP_DATABASE_USER} -p${LOOKUP_DATABASE_USER} < /vagrant/artifacts/lookup-server-${LOOKUP_SERVER_VERSION}/mysql.dmp

echo "END - Create the database"

cd /vagrant/artifacts/lookup-server-${LOOKUP_SERVER_VERSION}/ 

make release

mv /vagrant/artifacts/lookup-server-${LOOKUP_SERVER_VERSION}/server /var/www/lookup-server

cp /vagrant/artifacts/config.php /var/www/lookup-server/config/

sed -i "s|#LOOKUP_DATABASE_NAME#|${LOOKUP_DATABASE_NAME}|g" /var/www/lookup-server/config/config.php

sed -i "s|#LOOKUP_DATABASE_USER#|${LOOKUP_DATABASE_USER}|g" /var/www/lookup-server/config/config.php

cp /vagrant/artifacts/lookup-server.conf /etc/apache2/sites-available/lookup-server.conf

## Putting the machine hostname in the Apache site configuration file (lookup-server.conf)
sed -i "s|#MACHINE_HOSTNAME#|${MACHINE_HOSTNAME}|g" /etc/apache2/sites-available/lookup-server.conf

a2ensite lookup-server.conf

a2enmod ssl

systemctl restart apache2

systemctl status apache2

echo "${ip_address} ${MACHINE_HOSTNAME}" >> /etc/hosts

wget -O/tmp/lookup-server-status.txt https://${MACHINE_HOSTNAME}/index.php/status

cat /tmp/lookup-server-status.txt

rm /tmp/lookup-server-status.txt

end_time=`date`

echo "This container has IP (interface: ${NETWORK_INTERFACE}): ${ip_address}"

echo "provisioning started: ${start_time}" 
 
echo "provisioning ended: ${end_time}"
