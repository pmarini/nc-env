### Stand-alone Nextcloud server

#### Setup

* Assuming that the copy of the template is called `talk-hpb`, move to folder `talk-hpb`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `nextcloud-spreed-repository.list` | APT Source File for Talk HPB repositories |
| `nextcloud-spreed-signaling-server.conf` | Signaling Server configuration file |
| `talk-hpb-signaling-nginx-site.conf` | Nginx site configuration file|
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |


* Open `Vagrantfile` and fill in the values of the following parameters and variables:
   * `lxd.name`: The name of the container (it is recommended to give the same name as the folder, in this example `talk-hpb`)
   * `MACHINE_HOSTNAME`: The hostname of the container (it is recommended to give the same name as the folder, plus the domain, in this example `nc-instance.localenv.com`)
   * `NEXTCLOUD_URL`: URL of the Nextcloud instance to define this Talk HPB service on
   * `CUSTOMER_ID`: Customer ID to access the enterprise software repository of Talk HPB
* Create folder `log`
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or setting up LXD nameserver
* Start using your environment
