#!/bin/bash

timedatectl set-timezone Europe/Madrid

start_time=`date`
  
echo "provisioning started: ${start_time}" 

NETWORK_INTERFACE=eth0

hostnamectl set-hostname ${MACHINE_HOSTNAME}

# Print some information about the container OS
hostnamectl

# Print some information about the container timezone
timedatectl

#####################################################################
## Get the IP address into an environment variable. This command outputs 
## an empty variable if the network interface name is not ${NETWORK_INTERFACE}
#####################################################################
ip_address=`ip -4 addr show ${NETWORK_INTERFACE} | grep -oP '(?<=inet\s)\d+(\.\d+){3}'`

apt update

apt install -y ca-certificates wget nginx curl gnupg2

wget -O/tmp/sign.pub https://packages.struktur.de/nextcloud/spreed/${CUSTOMER_ID}/struktur-ag-deb-sign-1.pub

apt-key add /tmp/sign.pub

cp /vagrant/artifacts/nextcloud-spreed-repository.list /etc/apt/sources.list.d/

OS_DIST=$(lsb_release -cs)

sed -i "s|#OS_DIST#|${OS_DIST}|g" /etc/apt/sources.list.d/nextcloud-spreed-repository.list

sed -i "s|#CUSTOMER_ID#|${CUSTOMER_ID}|g" /etc/apt/sources.list.d/nextcloud-spreed-repository.list
apt update

apt install -y  nextcloud-spreed-services-full \
				mkcert

cp /vagrant/artifacts/nextcloud-spreed-signaling-server.conf /etc/nextcloud-spreed-signaling/server.conf

sed -i "s|#NEXTCLOUD_URL#|${NEXTCLOUD_URL}|g" /etc/nextcloud-spreed-signaling/server.conf

SECRET_KEY=`openssl rand -hex 32`

sed -i "s|#SECRET_KEY#|${SECRET_KEY}|g" /etc/nextcloud-spreed-signaling/server.conf

export CAROOT=/vagrant/artifacts/

mkcert -install

mkdir /etc/nginx/ssl/

mkcert  --cert-file /etc/nginx/ssl/${MACHINE_HOSTNAME}.pem --key-file  /etc/nginx/ssl/${MACHINE_HOSTNAME}-key.pem "${MACHINE_HOSTNAME}"

cp /vagrant/artifacts/talk-hpb-signaling-nginx-site.conf /etc/nginx/sites-available/talk-hpb-signaling-nginx-site.conf

ln -s /etc/nginx/sites-available/talk-hpb-signaling-nginx-site.conf /etc/nginx/sites-enabled/talk-hpb-signaling-nginx-site.conf

sed -i "s|#MACHINE_HOSTNAME#|${MACHINE_HOSTNAME}|g" /etc/nginx/sites-available/talk-hpb-signaling-nginx-site.conf

rm /etc/nginx/sites-enabled/default

systemctl restart nextcloud-spreed-signaling

systemctl restart nginx

## Test the Signaling Server
curl -i https://${MACHINE_HOSTNAME}/standalone-signaling/api/v1/welcome

## Check the status of the main services
systemctl status janus

systemctl status nextcloud-spreed-signaling

systemctl status nginx

end_time=`date`

echo "This container has IP (interface: ${NETWORK_INTERFACE}): ${ip_address}"

echo " The shared secret to be configured in the Nextcloud Talk UI is: ${SECRET_KEY}"

echo "provisioning started: ${start_time}" 
 
echo "provisioning ended: ${end_time}"
