#!/bin/bash

timedatectl set-timezone Europe/Madrid

start_time=`date`
  
echo "provisioning started: ${start_time}" 

#### The user must fill these variables - START

# Fully Qualified Name: ex. "self-hosted-appstore.localenv.com"
MACHINE_HOSTNAME=

#### The user must fill these variables - END
SYS_USER=usrv

NETWORK_INTERFACE=eth0

hostnamectl set-hostname ${MACHINE_HOSTNAME}

# Print some information about the container OS
hostnamectl

# Print some information about the container timezone
timedatectl

#####################################################################
## Get the IP address into an environment variable. This command outputs 
## an empty variable if the network interface name is not ${NETWORK_INTERFACE}
#####################################################################
ip_address=`ip -4 addr show ${NETWORK_INTERFACE} | grep -oP '(?<=inet\s)\d+(\.\d+){3}'`

adduser --disabled-password --gecos GECOS ${SYS_USER}
 
usermod -p "`openssl passwd -1  -salt 5RPVAd ${SYS_USER}`" ${SYS_USER}
 
adduser ${SYS_USER} sudo


## Install the needed packages from apt repositories
apt update

apt install -y apache2

## Install the mkcert rootCA (see https://github.com/FiloSottile/mkcert#installing-the-ca-on-other-systems) 
chmod u+x /vagrant/artifacts/mkcert

export CAROOT=/vagrant/artifacts/

/vagrant/artifacts/mkcert -install

/vagrant/artifacts/mkcert  --cert-file /etc/ssl/certs/${MACHINE_HOSTNAME}.pem --key-file /etc/ssl/private/${MACHINE_HOSTNAME}-key.pem "${MACHINE_HOSTNAME}"

## Apache Web Server Configuration

echo "START - Apache Web Server Configuration"

cp /vagrant/artifacts/appstore.conf /etc/apache2/sites-available/appstore.conf

## Putting the machine hostname in the Apache site configuration file (appstore.conf)
sed -i "s|#MACHINE_HOSTNAME#|${MACHINE_HOSTNAME}|g" /etc/apache2/sites-available/appstore.conf


## Use mkcert to install a locally trusted SSL certificate
chown root.ssl-cert  /etc/ssl/private/${MACHINE_HOSTNAME}-key.pem

chmod 640 /etc/ssl/private/${MACHINE_HOSTNAME}-key.pem

## Activate nextcloud.conf
a2ensite appstore.conf

## Disactivate 000-default.conf
a2dissite 000-default.conf


## Additional recommended modules
a2enmod ssl

mkdir /var/www/appstore

cp /vagrant/artifacts/apps.json /var/www/appstore

cp /vagrant/artifacts/categories.json /var/www/appstore

chown -R www-data:www-data /var/www/appstore/

echo "END - Apache Web Server Configuration"

systemctl restart apache2

systemctl status apache2

## Adding system user to www-data group (to ease command line operations)
adduser ${SYS_USER} www-data

echo "START - Cleanup"

echo "END - Cleanup"

end_time=`date`

echo "This container has IP (interface: ${NETWORK_INTERFACE}): ${ip_address}"

echo "If you add this IP to the hostname (${MACHINE_HOSTNAME}) in your hosts file:"

echo "	You can connect with user ${SYS_USER} via ssh (password ${SYS_USER}): ssh ${SYS_USER}@${MACHINE_HOSTNAME}"

echo "	You can specify the following value for appstoreurl in your Nextcloud instance: https://${MACHINE_HOSTNAME}"

echo "provisioning started: ${start_time}" 
 
echo "provisioning ended: ${end_time}"
