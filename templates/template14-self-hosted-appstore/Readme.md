### Self-Hosted Appstore

#### Setup

* Assuming that the copy of the template is called `self-hosted-appstore`, move to folder `self-hosted-appstore`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `apps.json` | JSON file for applications |
| `appstore.conf` | Apache Web Server Virtual Host file |
| `categories.json` | JSON file for application categories |
| `mkcert` | mkcert command line utility to be downloaded [here](https://github.com/FiloSottile/mkcert/releases). Pick the latest binary for amd64 and rename it to `mkcert`  |
| `rootCA.pem` | The rootCA previously created in your host machine |
| `rootCA-key.pem` | The rootCA key previously created in your host machine |



* Create folder `log`
* Open `Vagrantfile` and change the value of variable `lxd.name`. It makes sense to give the same name as the folder, in this example `self-hosted-appstore`.
* Open `provision.sh`
   * change the value of variable `MACHINE_HOSTNAME`. It makes sense to give the same name as the folder, plus the domain, in this example `self-hosted-appstore.localenv.com`.
* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment
