### Notify Push Server

#### Setup

* Assuming that the copy of the template is called `notify-push-server`, move to folder `notify-push-server`.
* Check the content of folder `artifacts`

| File name | Description|
| --- | --- |
| notify_push.env | Environment File for the systemd service |
| notify_push.service | Systemd service definition |
| notify_push | Binary to be downloaded from the [release page](https://github.com/nextcloud/notify_push/releases) and renamed |

* Create folder `log`
* Open `Vagrantfile` and change the value of variables and parameters

   * `lxd.name` (it is recommended to give the same name as the folder, in this example `notify-push-server`)
   * `MACHINE_HOSTNAME` (it is recommended to give the same name as the folder, plus the domain, in this example `notify-push-server.localenv.com`)
   * `NEXTCLOUD_URL`
   * `DATABASE_URL`. Format: <db_type>://<db_user>:<db_password>@<db_host>:<db_port>/<db_name>, example with db_type=mysql, db_user=nextcloud_usr, db_password=nextcloud_usr, db_host=localhost, dbport=3306, db_name=nextcloud_db: mysql://nextcloud_usr:nextcloud_usr@localhost:3306/nextcloud_db
   * `REDIS_URL`. Example: redis://localhost:6379
   * `DATABASE_PREFIX` (optional)
   * `ALLOW_SELF_SIGNED` (optional)
   * `PORT` (optional)

* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts`
* Start using your environment

