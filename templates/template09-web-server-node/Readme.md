### Stand-alone Nextcloud node (to be used in a cluster)

#### Setup

* Assuming that the copy of the template is called `nc-instance`, move to folder `nc-instance`.
* Check the content of folder `artifacts`

|File name | Description|
| --- | --- |
| `nextcloud-X.Y.Z.tar.bz2` | The installer archive to be downloaded from [here](https://download.nextcloud.com/server/releases/) |
| `nextcloud-X.Y.Z.tar.bz2.md5` | md5 signature of the installer archive to be downloaded from [here](https://download.nextcloud.com/server/releases/) |
| `nextcloud.conf` | Apache Web Server main configuration file |
| `data-nextcloud.mount` | Mount systemd service for the GlusterFS filesystem |
| `occ` | Alias for the occ command |


* Create folder `log`
* Open `Vagrantfile` and assign a value to the following variables and parameters:

   * `lxd.name` (it is recommended to give the same name as the folder, in this example `nc-instance`)
   * `MACHINE_HOSTNAME` 
   * `NEXTCLOUD_INSTALLER_ARCHIVE` to match the name of the installer archive you are using.
   * `DATABASE_MACHINE_HOSTNAME`
   * `DATABASE_ALREADY_EXIST`
   * `REDIS_MACHINE_HOSTNAME`
   * `REDIS_MACHINE_PORT`
   * `GLUSTERFS_MACHINE_HOSTNAME`
   * `GLUSTERFS_VOLUME`

* Run `vagrant up > log/provisioning.log`
* Make sure your system is able to resolve the domain name that you specified in variable `MACHINE_HOSTNAME`, for example by adding an entry in `/etc/hosts` or configure LXD nameserver
* Start using your environment
